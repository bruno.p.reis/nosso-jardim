<?php

use Tests\AbstractFunctionalTestCase;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use AppBundle\Entity\User;

class AuthorizationTest extends AbstractFunctionalTestCase
{
    /**
     * @return string
     */
    protected function getGraphQlQuery()
    {
        $query = <<<gql
{
  messages {
    edges {
      node {
        id
      }
    }
  }
}
gql;
        return $query;
    }

    /**
     * @test
     */
    public function shouldNotAccessGraphQlApiWithoutToken()
    {
        $client = static::createClient();
        $client->request('POST', '/graphql', [], [], [], $this->getGraphQlQuery());
        $this->assertInstanceOf(JWTAuthenticationFailureResponse::class, $client->getResponse());
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function shouldAccessGraphQlApiWithToken()
    {
        $user = User::fromTelegram('qwerty', 'Foo Bar Xpto');
        $this->em->persist($user);
        $this->em->flush();

        $token = $this->container->get('lexik_jwt_authentication.jwt_manager')->create($user);

        $server = [
            'CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer {$token}"
        ];

        $client = static::createClient();
        $client->request('POST', '/graphql', ['query' => $this->getGraphQlQuery()], [], $server);

        $responseData = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('data', $responseData);
        $this->assertArrayHasKey('messages', $responseData['data']);
        $this->assertArrayHasKey('edges', $responseData['data']['messages']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
