<?php
namespace Tests\AppBundle\GraphQL\Subtopics;
use AppBundle\Entity\Subtopic;
use Tests\AppBundle\GraphQL\TestHelper;

class SubtopicsTestHelper extends TestHelper{


    function SUBTOPIC_DELETE($args = [], $getError = false, $initialPath = 'subtopicsDelete') {
        $q ='mutation subtopicsDelete(
                $id:ID!
            ){ 
                subtopicsDelete(
                    id:$id
                ) {
                        id,
                        subtopics{
                            id
                        }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function SUBTOPIC_EDIT($args = [], $getError = false, $initialPath = 'subtopicsEdit') {
        $q = 'mutation subtopicsEdit(
                $id:ID!
                $name:String
                $canHaveItems:Boolean
            ){ 
                subtopicsEdit(
                    id:$id
                    name:$name
                    canHaveItems:$canHaveItems
                ) {
                    id
                    subtopics{
                        id
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    public function SUBTOPICS_QUERY($args = [], $getError = false, $initialPath = 'subtopics') {
        $q ='query subtopics(
                $canHaveItems:Boolean, 
                $root:Boolean
            ){
                subtopics(
                    root:$root
                    canHaveItems: $canHaveItems
                ){
                    id
                    name
                    canHaveItems
                    ancestors {id,name}
                    subtopics{
                        id
                        name
                        canHaveItems
                        ancestors {id,name}
                        subtopics{
                            id
                            name
                            canHaveItems
                            ancestors {id,name}
                            subtopics{
                                id
                                name
                                canHaveItems
                                ancestors {id,name}
                            }
                        }
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    function SUBTOPIC_MOVE($args = [], $getError = false, $initialPath = 'subtopicsMove') {
        if(!$args['offset']) {
            $args['offset'] = 0;
        }
        $q ='mutation subtopicsMove(
                $id:ID!,
                $parentId:ID!,
                $offset:Int!
            ){ 
                subtopicsMove(
                    id:$id,
                    parentId:$parentId,
                    offset:$offset
                ) {
                    id,
                    name,
                    subtopics{
                        id,name,subtopics{
                            id,name,subtopics{
                                id,name
                            }
                        }
                    }
                }
            }';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }

    public function SUBTOPICS_REGISTER($args = [], $getError = false, $initialPath='subtopicsRegister') {
        $q ='mutation subtopicsRegister(
                    $parentId: ID!
                    $name: String
                ){
                    subtopicsRegister(
                        parentId:$parentId
                        name:$name
                ){
                    id,
                    name,
                    canHaveItems
                    subtopics{
                        id,
                        name,
                        canHaveItems
                        subtopics{
                            id,
                            name,
                            canHaveItems
                            subtopics{
                                id,
                                name,
                                canHaveItems,
                                subtopics{
                                    id,
                                    name,
                                    canHaveItems
                                }
                            }
                        }
                    }
                }}';
        return $this->runner->processResponse($q,$args,$getError,$initialPath);
    }
}