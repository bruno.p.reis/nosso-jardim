<?php
namespace Tests\AppBundle\GraphQL\Subtopics\Mutations;

use Tests\AppBundle\GraphQL\Subtopics\SubtopicsTestHelper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;

class EditTest extends AbstractIntegrationTestCase
{
    function helper() {
        return new SubtopicsTestHelper($this);
    }
    
    /** @test */
    public function shouldChangeSubtopicData()
    {
        $h = $this->helper();

        $subtopicId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL([
            'name'=>'Novo'
        ])('0.subtopics.0.id');

        $h->SUBTOPIC_EDIT([
            'id'=>$subtopicId,
            'name'=>'Novo Nome',
            'canHaveItems'=>true
        ]);
        $subtopic = $h->SUBTOPICS_QUERY(['root'=>true])('0.subtopics.0');
        
        $this->assertEquals('Novo Nome',$subtopic['name']);
        $this->assertEquals(true,$subtopic['canHaveItems']);
    }
    

}
