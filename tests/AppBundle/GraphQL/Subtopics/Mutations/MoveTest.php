<?php
namespace Tests\AppBundle\GraphQL\Subtopics\Mutations;

use Tests\AppBundle\GraphQL\Subtopics\SubtopicsTestHelper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;

class MoveTest extends AbstractIntegrationTestCase
{
    function helper() {
        return new SubtopicsTestHelper($this);
    }

    /** @test */
    public function shouldChangeParent()
    {
        $h = $this->helper();

        $fertilizationId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>'Fertilization'])
            ('0.subtopics.0.id');

        $irrigationId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>'Irrigation'])
            ('0.subtopics.1.id');
        $compostingId = $h->SUBTOPICS_REGISTER([
            'parentId'=>$irrigationId,
            'name'=>'Composting'
        ])('0.subtopics.1.subtopics.0.id');
        
        
        $h->SUBTOPIC_MOVE([
            'id' => $compostingId,
            'parentId' => $fertilizationId,
            'offset' => 0
        ]);

        $subtopics = $h->SUBTOPICS_QUERY(['root'=>true])('0.subtopics');        

        $this->assertCount(0,$subtopics[1]['subtopics'],'Irrigation shall not have a child');
        $this->assertCount(1,$subtopics[0]['subtopics'],'Fertilization shall have a child');
        $this->assertEquals('Fertilization',$subtopics[0]['name']);
        $this->assertEquals($compostingId,$subtopics[0]['subtopics'][0]['id']);
    }

    /** @test */
    public function shouldChangeParentToRootIfParentIdIsZero()
    {
        $h = $this->helper();
        $h->initRoot();

        $fertilizationId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(
            ['name'=>'Fertilization']
        )('0.subtopics.0.id');

        $irrigationId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(
            ['name'=>'Irrigation']
        )('0.subtopics.1.id');

        $compostingId = $h->SUBTOPICS_REGISTER([
            'parentId'=>$irrigationId,
            'name'=>'Composting'
        ])('0.subtopics.1.subtopics.0.id');
       
        $h->SUBTOPIC_MOVE([
            'id' => $compostingId,
            'parentId' => 0,
            'offset' => 0
        ]);

        $subtopics = $h->SUBTOPICS_QUERY(['root'=>true])('0.subtopics');

        $this->assertCount(0,$subtopics[1]['subtopics'],'Irrigation shall not have a child');
        $this->assertCount(3,$subtopics);
    }

    /** @test */
    public function shouldPutInCorrectOrder()
    {
        $h = $this->helper();
        $h->initRoot();

        $plantsId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(
            ['name'=>'Plants']
        )('0.subtopics.0.id');
        $grapeId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(
            ['name'=>'Grape']
        )('0.subtopics.1.id');

        $avocadoId = $h->SUBTOPICS_REGISTER([
            'parentId'=>$plantsId,
            'name'=>'Avocado'
        ])('0.subtopics.0.subtopics.0.id');
        $berryId = $h->SUBTOPICS_REGISTER([
            'parentId'=>$plantsId,
            'name'=>'Berry'
        ])('0.subtopics.0.subtopics.1.id');
        $carrotId = $h->SUBTOPICS_REGISTER([
            'parentId'=>$plantsId,
            'name'=>'Carrot'
        ])('0.subtopics.0.subtopics.2.id');
       
        
        $h->SUBTOPIC_MOVE([
            'id' => $grapeId,
            'parentId' => $plantsId,
            'offset' => 1
        ]);

        $subtopics = $h->SUBTOPICS_QUERY(['root'=>true])('0.subtopics'); 

        $this->assertCount(1,$subtopics);
        $this->assertCount(4,$subtopics[0]['subtopics']);
        $this->assertEquals('Grape',$subtopics[0]['subtopics'][1]['name']);

        $h->SUBTOPIC_MOVE([
            'id' => $grapeId,
            'parentId' => $plantsId,
            'offset' => 0
        ]);
      
        $subtopics = $h->SUBTOPICS_QUERY(['root'=>true])('0.subtopics'); 

        $this->assertEquals(
            'Grape',
            $subtopics[0]['subtopics'][0]['name']
        );

        $h->SUBTOPIC_MOVE([
            'id' => $grapeId,
            'parentId' => $plantsId,
            'offset' => 3
        ]);

        $subtopics = $h->SUBTOPICS_QUERY(['root'=>true])('0.subtopics');

        $this->assertEquals('Grape',$subtopics[0]['subtopics'][3]['name']);
    }
    

}
