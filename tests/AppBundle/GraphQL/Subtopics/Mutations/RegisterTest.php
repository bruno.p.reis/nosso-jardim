<?php
namespace Tests\AppBundle\GraphQL\Subtopics\Mutations;

use Tests\AppBundle\GraphQL\Subtopics\SubtopicsTestHelper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\Entity\Subtopic;


class RegisterTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new SubtopicsTestHelper($this);
    }

    /** @test */
    public function shouldCreateAFirstLevelSubtopic()
    {

        $h = $this->helper();

       
        $subtopicId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL([
                'name'=>"Fertilization"
        ])('0.id');

        
        $firstSubName = $h->SUBTOPICS_QUERY(['root'=>true])
        ('0.subtopics.0.name');

        $this->assertEquals('Fertilization',$firstSubName);
    }

    /** @test */
    public function shouldRegisterAnInnerSubtopic()
    {
        $h = $this->helper();

        $subtopicId = $h->SUBTOPICS_REGISTER_FIRST_LEVEL([
            'name'=>"Fertilization"
        ])('0.subtopics.0.id');

        $h->SUBTOPICS_REGISTER([
            'parentId' => $subtopicId,
            'name' => "Composting"
        ])();
        
        $sub = $h->SUBTOPICS_QUERY(
            ['root'=>true]
        )('0.subtopics.0.subtopics.0');
        
        $this->assertEquals('Composting',$sub['name']);
    }

}
