<?php
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\Resolver\Mutation\RegisterTelegramDumperMessageResolver;
use AppBundle\Entity\Message;
use AppBundle\Entity\Thread;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class ThreadAddMessagesTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }

    /**
     * @test
     */
    public function shouldAddOneMessage()
    {
        $h = $this->helper();

        $time = time();
        
        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?', 'date' => ++$time], 
            ['registerTelegramDumperMessage', 'id']
        );
        
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo.', 'date' => ++$time], 
            ['registerTelegramDumperMessage','id']
        );
        
        $threadId = $h->THREAD_REGISTER([
            'messageIds'=>[$messageId1]
        ])('id');

        $h->THREAD_ADD_MESSAGES([
            'messageIds'=>[$messageId2],
            'threadId'=>$threadId
        ]);

        $ids = $h->THREAD([
            'id'=>$threadId
        ])('thread.messages.*.id','...');
        
        $this->assertCount(
            2, 
            $ids,
            'a thread deve ficar com duas mensagens'
        );

        $this->assertEquals(
            [$messageId1,$messageId2], 
            $ids,
            'a thread deve ficar com as duas mensagens corretas'
        );
    }

    /**
     * @test
     */
    public function shouldAddTwoMessages()
    {
        $time = time();
        $h = $this->helper();

        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?', 'date' => ++$time],['registerTelegramDumperMessage','id']
        );
        
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo', 'date' => ++$time],['registerTelegramDumperMessage','id']
        );
        
        $messageId3 = $h->registerTelegramDumperMessage(
            ['text'=>'Então tá bom.', 'date' => ++$time],['registerTelegramDumperMessage','id']
        );

        $threadId = $h->THREAD_REGISTER([
            'messageIds'=>[$messageId1]
        ])('id');

        $h->THREAD_ADD_MESSAGES([
            'messageIds'=>[$messageId2,$messageId3],
            'threadId'=>$threadId
        ]);

        $messages = $h->THREAD([
            'id'=>$threadId
        ])('thread.messages.*.id','...');

        $expected = [$messageId1,$messageId2,$messageId3];
      
        $this->assertEquals(
            $expected, 
            $messages,
            'a thread deve ficar com as três mensagens corretas'
        );
    }

    /**
     * @test
     */
    public function shouldReturnAnErrorIfAMessageDoesNotExists()
    {
        $time = time();
        $h = $this->helper();
        
        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?', 'date' => ++$time],['registerTelegramDumperMessage','id']
        );
        
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo', 'date' => ++$time],['registerTelegramDumperMessage','id']
        );

        $threadId = $h->THREAD_REGISTER([
            'messageIds'=>[$messageId1]
        ])('id');

        $error = $h->THREAD_ADD_MESSAGES([
            'messageIds'=>["999"],
            'threadId'=>$threadId
        ],true);

        
    }

}
