<?php
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\Resolver\Mutation\RegisterTelegramDumperMessageResolver;
use AppBundle\Entity\Message;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class MessagesMarkIrrelevantTest extends AbstractIntegrationTestCase
{
    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }
    
    /**
     * @test
     */
    public function shouldMarkIrrelevantMessages()
    {
        $h = $this->helper();

        $mId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?'],
            ['registerTelegramDumperMessage','id']
        );
        $mId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo'],
            ['registerTelegramDumperMessage','id']
        );
        $mId3 = $h->registerTelegramDumperMessage(
            ['text'=>'All righ'],
            ['registerTelegramDumperMessage','id']
        );

        $h->MESSAGES_MARK_IRRELEVANT([
            'messageIds'=> [ $mId1 , $mId3 ]
        ]);

        $message1 = $this->gql_searchMessageById($mId1);
        $this->assertTrue(
            $message1['irrelevant'],
            'm1 should be marked as irrelevant'
        );

        $message2 = $this->gql_searchMessageById($mId2);
        $this->assertFalse(
            $message2['irrelevant'],
            'm2 should not be marked as irrelevant'
        );

        $message3 = $this->gql_searchMessageById($mId3);
        $this->assertTrue(
            $message3['irrelevant'],
            'm3 should be marked as irrelevant'
        );

    }
}
