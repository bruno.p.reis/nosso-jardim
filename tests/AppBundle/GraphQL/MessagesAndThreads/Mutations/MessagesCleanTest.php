<?php
use Tests\AbstractIntegrationTestCase;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class MessagesCleanTest extends AbstractIntegrationTestCase
{

    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }

    /**
     * @test
     */
    public function shouldCleanThreadFromMessages()
    {
        $time = time();
        $h = $this->helper();
        
        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );
        
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );
        
        $messageId3 = $h->registerTelegramDumperMessage(
            ['text'=>'All righ', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );
        
        $threadId = $h->THREAD_REGISTER([
            'messageIds'=>[$messageId1, $messageId2, $messageId3]
        ])('id');
        
        $h->MESSAGES_CLEAN([
            'messageIds'=>[ 
                $messageId1 , 
                $messageId3 
            ]
        ]);
 
        $messages = $h->THREAD([
            'id'=>$threadId
        ])('thread.messages');

        $this->assertCount(
            1, 
            $messages,
            'a thread inserida deve ter um array com uma mensagem apenas'
        );

        $this->assertEquals(
            $messageId2, 
            $messages[0]['id'],
            'deve sobrar só a mensagem dois'
        );
    }

    /**
     * @test
     */
    public function shouldCleanIrrelevantMessages()
    {
        $time = time();
        $h = $this->helper();
        
        $mId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );
        
        $mId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );
        
        $mId3 = $h->registerTelegramDumperMessage(
            ['text'=>'All righ', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );

        $h->MESSAGES_MARK_IRRELEVANT([
            'messageIds'=> [ $mId1 , $mId3 ]
        ]);

        $h->MESSAGES_CLEAN([
            'messageIds'=>[ 
                $mId3 
            ]
        ]);
        
        $message1 = $this->gql_searchMessageById($mId1);
        $this->assertTrue($message1['irrelevant'],'m1 should be marked as irrelevant');

        $message2 = $this->gql_searchMessageById($mId2);
        $this->assertFalse($message2['irrelevant'],'m2 should not be marked as irrelevant');

        $message3 = $this->gql_searchMessageById($mId3);
        $this->assertFalse($message3['irrelevant'],'m3 should be cleaned. Not irrelevant anymore.');
    }

    /**
     * @test
     */
    public function shouldRemoveEmptyThread()
    {
        $time = time();
        $h = $this->helper();
        
        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );
        
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo', 'date' => ++$time],
            ['registerTelegramDumperMessage','id']
        );
        
        
        $threadId = $h->THREAD_REGISTER([
            'messageIds'=>[$messageId1,$messageId2]
        ])('id');

        $h->MESSAGES_CLEAN([
            'messageIds'=>[ 
                $messageId1
            ]
        ]);

         
        $this->assertCount(
            1,
            $h->THREADS([
                'id'=>$threadId
            ])()
        );

        
        $h->MESSAGES_CLEAN([
            'messageIds'=>[ 
                $messageId2 
            ]
        ]);

        
        $threads = $h->THREADS([
            'id'=>$threadId
        ])();

        $this->assertCount(0,$threads);
    }
}
