<?php

use Tests\AbstractIntegrationTestCase;
use AppBundle\DataFixtures\ORM\LoadMediaTypeData;
use Tests\AppBundle\GraphQL\MessagesAndThreads\MessagesAndThreadsTestHelper;

class ThreadsTest extends AbstractIntegrationTestCase
{   

    function helper() {
        return new MessagesAndThreadsTestHelper($this);
    }

    private function registerThread($messageId) {
        $h = $this->helper();

        return $h->THREAD_REGISTER([
            'messageIds'=>[$messageId]
        ])('id');
    }

    /**  @test */
    public function shouldFilterByPeer()
    {
        $h = $this->helper();
       
        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?'],
            ['registerTelegramDumperMessage','id']
        );
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo'],
            ['registerTelegramDumperMessage','id']
        );
        $messageId3 = $h->registerTelegramDumperMessage(
            ['text'=>'All righ'],
            ['registerTelegramDumperMessage','id'],
            'SEMENTES'
        );

        $threadId1 = $this->registerThread($messageId1);
        $threadId2 = $this->registerThread($messageId2);
        $threadId3 = $this->registerThread($messageId3);

       
        $this->assertCount(
            2,
            $h->THREADS(['peerId'=>1])()
        );
        $this->assertCount(
            1,
            $h->THREADS(['peerId'=>2])()
        );
    }

    /**  @test */
    public function shouldLoadANonExistentThreadWithError()
    {
        $h = $this->helper();
       
        $messageId1 = $h->registerTelegramDumperMessage(
            ['text'=>'Olá, tudo bem?'],
            ['registerTelegramDumperMessage','id']
        );
        $messageId2 = $h->registerTelegramDumperMessage(
            ['text'=>'Tudo'],
            ['registerTelegramDumperMessage','id']
        );
        

        $threadId1 = $this->registerThread($messageId1);
        $error = $h->THREAD(['id'=>33],true);
        

    }
}
