<?php
namespace Tests\AppBundle\GraphQL\Items\Mutations;
use Tests\AppBundle\GraphQL\Items\ItemTestHelper;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\AbstractIntegrationTestCase;
use AppBundle\Entity\Item;
use AppBundle\Entity\Area;

class ItemsEditTest extends AbstractIntegrationTestCase
{
    function helper() {
        return new ItemTestHelper($this);
    }

    /**
     * @test
     */
    public function shouldCreateOneItemAndEditName()
    {
        $h = $this->helper();
        $subtopic1Id = $h->SUBTOPICS_REGISTER_FIRST_LEVEL(['name'=>"Planta"])('0.id');
        $r = $h->SUBTOPICS_EDIT([
            'id'=>$subtopic1Id,
            'canHaveItems'=>true
        ]);
        $item1Id = $h->addItem("Mogno",$subtopic1Id);
        $this->assertTrue($item1Id == intval($item1Id));
        $newName = "Bananeira";
        $item1Id = $this->itemsEdit(
            $item1Id,
            $newName,
            $subtopic1Id
        );
        $this->assertEquals(
            'Bananeira',
            $h->ITEMS_QUERY()('0.name')
        );
    }

    public function itemsEdit($itemId, $name){
        $h = $this->helper();
        return $h->ITEMS_EDIT([
            'id'=>$itemId,
            'name'=>$name
        ])('id');
    }
}