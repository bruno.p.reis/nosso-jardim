<?php

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\DatabaseFileManager;


abstract class AbstractIntegrationTestCase extends KernelTestCase
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var DatabaseFileManager
     */
    private $dbFileManager;

    protected function setUp()
    {
        parent::setUp();

        self::bootKernel();

        $this->container = static::$kernel->getContainer();

        $this->em = $this->container->get('doctrine.orm.entity_manager');

        $databaseFile = $this->em->getConnection()->getDatabase();

        $this->dbFileManager = new DatabaseFileManager(
            $databaseFile,
            $databaseFile . '.bak',
            new Application(static::$kernel)
        );

        $this->dbFileManager->createDatabase();

        $this->runner = new \Reis\GraphQLTestRunner\Runner\Runner(
            $this->getService('overblog_graphql.request_executor'),
            $this
        );
    }

    public function gql($query, $pathToReturnData = null){
        return $this->runner->gqlData($query, $pathToReturnData);
    }

    public function em() {
        return $this->em;
    }

    /**
     * @param string $service
     * @return mixed
     */
    protected function getService($service)
    {
        return $this->container->get($service);
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
        $this->em = null;
        $this->dbFileManager->restoreDatabase();
    }
    
    /**
     * @return \Faker\Factory
     */
    protected function getFaker()
    {
        return $this->getService('faker.generator');
    }

    /**
     * @param string $query
     * @param array $expectedArray
     */
    protected function assertGraphQL($query, array $expectedArray)
    {
        $processor = $this->getService('graphql.processor');
        $processor->processPayload($query);

        if (!isset($expectedArray['data']) && !isset($expectedArray['errors'])) {
            $expectedArray = ['data' => $expectedArray];
        }

        $this->assertEquals($processor->getResponseData(), $expectedArray);
    }
    


    /**
     * Busca mensagem na api e retorna opcionalmente somente as informações 
     * da mensagem, desconsiderando os dados da paginação
     * @param integer $id
     * @param boolean $returnNode
     * SERÁ ISSO VÁLIDO MESMO? 
     */
    public function gql_searchMessageById($id, $returnNode = true)
    {
        $query = <<<gql
{
  messages (id: $id) {
    edges {
      node {
        id
        sender {
          id
          name
          email
          telegramId
          createdAt
        }
        thread {
          id
        }
        text
        irrelevant
        telegramId
        sentAt
        createdAt
      }
      cursor
    }    
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
  } 
}            
gql;
        
        $data = $this->runner->gqlData($query);
        
        if ($returnNode) {
            return $data['messages']['edges'][0]['node'];
        }
        
        return $data;
    }
}
