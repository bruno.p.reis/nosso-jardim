<?php

namespace Tests;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class DatabaseFileManager
{   
    /**
     * @var string
     */
    protected $databaseFile;
    
    /**
     * @var string
     */
    protected $databaseFileBackup;
    
    /**
     * @var Application
     */
    protected $application;
    
    /**
     * @param string $databaseFile
     * @param string $databaseFileBackup
     * @param Application $application
     */
    public function __construct($databaseFile, $databaseFileBackup, $application)
    {
        $this->databaseFile = $databaseFile;
        $this->databaseFileBackup = $databaseFileBackup;
        $this->application = $application;
    }

    public function createDatabase()
    {
        if (!file_exists($this->databaseFile)) {
            
            $this->application->setAutoExit(false);

            $schemaCreate = [
                'command' => 'doctrine:schema:create',
                '--quiet' => true,
                '--env' => 'test'
            ];
            
            $this->application->run(new ArrayInput($schemaCreate));

            $this->backupDatabase();
        }
    }
    
    public function backupDatabase()
    {
        copy($this->databaseFile, $this->databaseFileBackup);
    }
    
    public function restoreDatabase()
    {
        unlink($this->databaseFile);
        copy($this->databaseFileBackup, $this->databaseFile);
    }
}
