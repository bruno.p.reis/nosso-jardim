<?php
namespace Reis\GraphqlGenBundle\Generator;

use Sensio\Bundle\GeneratorBundle\Generator\Generator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Doctrine\Common\Inflector\Inflector;

class GraphQLMutationGenerator extends Generator
{
    private $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function generate(BundleInterface $bundle, $namespace, $name, $type, $description)
    {
        $bundleDir = $bundle->getPath();
        $mutationDir = $bundleDir.'/GraphQL/'.$namespace.'/Mutations';
        $mutationResolverDir = $bundleDir.'/Resolver/'.$namespace.'/Mutations';
        $mutationTestDir = 'tests/'.$bundle->getNamespace().'/GraphQL/'.$namespace.'/Mutations';
        self::mkdir($mutationDir);
        self::mkdir($mutationResolverDir);
        self::mkdir($mutationTestDir);

        $mutationClassName = $namespace . $this->classify($name);
        $mutationResolverClassName = $namespace . $this->classify($name);
        $mutationTestClassName = $this->classify($name) . 'Test';

        $mutationFile = $mutationDir.'/'.$mutationClassName.'.php';
        $mutationResolverFile = $mutationResolverDir.'/'.$mutationResolverClassName.'.php';
        $mutationTestFile = $mutationTestDir.'/'.$mutationTestClassName.'.php';

        if ($this->filesystem->exists($mutationFile)) {
            throw new \RuntimeException(sprintf('Mutation "%s" already exists', $mutationFile));
        }
        if ($this->filesystem->exists($mutationResolverFile)) {
            throw new \RuntimeException(sprintf('Mutation Revolver "%s" already exists', $mutationResolverFile));
        }
        if ($this->filesystem->exists($mutationTestFile)) {
            throw new \RuntimeException(sprintf('Mutation Test "%s" already exists', $mutationTestFile));
        }

        $parameters = array(
            'inner_namespace' => $namespace,
            'namespace' => $bundle->getNamespace(),
            'class_name' => $mutationClassName,
            'mutation_name' => Inflector::camelize($mutationClassName),
            'test_class_name' => $mutationTestClassName,
            'service_name' => Inflector::tableize($namespace) . '.' . Inflector::tableize($name),
            'name' => $name,
            'type' => $type,
            'description' => $description
        );

        $this->setSkeletonDirs('src/Reis/GraphqlGenBundle/Generator/skeleton');

        $this->renderFile('Mutation.php.twig', $mutationFile, $parameters);
        $this->addMutationToSchema($bundleDir,$parameters);
        $this->generateResolverServiceRegistration($bundleDir,$parameters);
        $this->renderFile('MutationResolver.php.twig', $mutationResolverFile, $parameters);
        $this->renderFile('MutationTest.php.twig', $mutationTestFile, $parameters);
    }

    function generateResolverServiceRegistration($bundleDir,$parameters) {
        $servicesFile = $bundleDir . '/Resources/config/mutation.yml';
        $fileContent = file_get_contents($servicesFile);
        $className = sprintf(
                "%s\Resolver\%s\Mutations\%s",
                $parameters['namespace'],
                $parameters['inner_namespace'],
                $parameters['class_name']
        );
        $fileContent .= "\n"
        ."    app.resolver.".$parameters['service_name'].":\n"
        ."        class: $className\n"
        ."        arguments:\n"
        ."            - '@doctrine.orm.entity_manager'";

        self::dump($servicesFile,$fileContent);
    }

    function addMutationToSchema($bundleDir, $parameters) {
        $schemaFile = $bundleDir . '/GraphQL/Schema.php';
        $fileContent = file_get_contents($schemaFile);

        $openingPos = strpos($fileContent,'$config->getMutation()->addFields([');
        $pos = strpos($fileContent,'])',$openingPos);
        $fileContent = $this->insertText(
            $fileContent,
            $pos,
            sprintf("    new %s(),\n        ",$parameters['class_name'])
        );

        $classPos = strpos($fileContent,'class');

        $fileContent = $this->insertText(
            $fileContent,
            $classPos,
            sprintf(
                "use %s\GraphQL\%s\Mutations\%s;\n",
                $parameters['namespace'],
                $parameters['inner_namespace'],
                $parameters['class_name'])
        );

        self::dump($schemaFile,$fileContent);
    }

    function insertText($fileContent,$pos,$text) {
        $initialPart = substr($fileContent, 0, $pos);
        $finalPart = substr($fileContent, $pos);
        return $initialPart . $text . $finalPart;
    }

    /**
     * Transforms the given string to a new string valid as a PHP class name
     * ('app:my-project' -> 'AppMyProject', 'app:namespace:name' -> 'AppNamespaceName').
     *
     * @param string $string
     *
     * @return string The string transformed to be a valid PHP class name
     */
    public function classify($string)
    {
        return str_replace(' ', '', ucwords(strtr($string, '_-:', '   ')));
    }
}
