<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=60, nullable=true)
     */
    private $password;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telegram_id", type="string", length=191, nullable=true, unique=true)
     */
    private $telegramId;
    
    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="sender")
     */
    private $messages;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var Media
     * 
     * @ORM\ManyToOne(targetEntity="Media")
     * @ORM\JoinColumn(name="photo_id" ,referencedColumnName="id", nullable=true)
     */
    private $photo;

    /**
     * @var ArrayCollection|Role[]
     *
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(
     *   name="users_role",
     *   joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $roles;

    /**
     * @param string $telegramId
     * @param string $name
     */
    public static function fromTelegram($telegramId, $name)
    {
        $user = new User();
        $user->telegramId = $telegramId;
        $user->name = $name;
        $user->messages = new ArrayCollection();
        $user->roles = new ArrayCollection();
        $user->createdAt = new \DateTime();
        return $user;
    }
    
    /**
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public static function fromRegistration($name, $email, $password)
    {
        $user = new User();        
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;   
        $user->messages = new ArrayCollection();
        $user->roles = new ArrayCollection();
        $user->createdAt = new \DateTime();
        return $user;        
    }
    
    /**
     * forçar a criação por meio de named constructors
     */
    private function __construct(){}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * @return string
     */
    public function getTelegramId()
    {
        return $this->telegramId;
    }

    /**
     * @return ArrayCollection|Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    /**
     * @return Media
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    
    /**
     * @param Media $photo
     */
    public function setPhoto(Media $photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->password = null;
    }

    /**
     * {@inheritdoc}
     */    
    public function getRoles()
    {
        $roles = [Role::USER];
        foreach ($this->roles as $role) {
            $roles[] = $role->getName();
        }
        return array_unique($roles);
    }

    /**
     * @param Role $role
     * @return User
     */
    public function addRole(Role $role)
    {
        $this->roles[] = $role;
        return $this;
    }

    /**
     * @param Role $role
     * @return User
     */
    public function removeRole(Role $role)
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */    
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */    
    public function getUsername()
    {
        return $this->telegramId;
    }

    /**
     * @return boolean
     */
    public function isAdministrator()
    {
        return false !== array_search(Role::ADMIN, $this->getRoles());
    }

    /**
     * @return boolean
     */
    public function isClassifier()
    {
        return false !== array_search(Role::CLASSIFIER, $this->getRoles());
    }
}
