<?php

namespace AppBundle\Repository;

class ItemRepository extends AbstractRepository
{
    public function findByIds($itemIds) {
        $items = $this->findById($itemIds);

        if(count($items) !== count($itemIds)) {
            $error = 'Some items were not found on the system.'
                .' You asked for ids [' . implode(',',$itemIds) . '] but'
                .' we have found only [' . implode(
                    ',',
                    array_map(
                        function($m){return $m->getId();},
                        $items
                    )
                ).']';
            throw new \RuntimeException($error);
        }

        return $items;
    }
}
