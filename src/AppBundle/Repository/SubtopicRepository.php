<?php

namespace AppBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class SubtopicRepository extends NestedTreeRepository {

	public function getFirstLevelNodes() {
		return $this->findByLvl(1);
	}
}
