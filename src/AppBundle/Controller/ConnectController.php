<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ConnectController extends Controller
{
    /**
     * @Route("/connect", name="connect")
     * @Method({"POST"})
     */
    public function indexAction(Request $request)
    {
        try {
            $this->get('app.service.connect')->sendAccessLink($request->getContent());
            return new Response('Ok');
            
        } catch (\InvalidArgumentException $e) {
            # retorna 200 para o servidor do telegram não persistir 
            # enviando as mensagens inválidas
            return new Response($e->getMessage(), 200);
        }
    }
}
