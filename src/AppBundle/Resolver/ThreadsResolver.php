<?php
namespace AppBundle\Resolver;

use Symfony\Component\HttpFoundation\ParameterBag;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Thread;

class ThreadsResolver extends EmAwareResolver
{
    public function repo($entityName = 'AppBundle:Thread') {
        return parent::repo($entityName);
    }

    public function addMessages($data)
    {
        $messageIds = $data['messageIds'];
        $threadId = $data['threadId'];

        $thread = $this->repo()->find($threadId);

        $messages = $this->repo('AppBundle:Message')->findByIds($messageIds);

        foreach($messages as $message) {
            $thread->addMessage($message);
        }
        $this->em->persist($thread);
        $this->em->flush();
        return $thread;
    }

    public function register($data)
    {
        $messageIds = $data['messageIds'];
        
        if(!count($messageIds) > 0) {
            throw new \Exception("We need at least one message", 1);
        }

        $messages = $this->repo('AppBundle:Message')->findByIds($messageIds);

        $thread = new Thread;
        foreach($messages as $message) {
            $thread->addMessage($message);
        }
        $this->em->persist($thread);
        $this->em->flush();
        
        return $thread;
    }

    public function findOne($data)
    {
        $id = $data['id'];
        $res = $this->repo()->find($id);
        if(!$res) {
            throw new \Overblog\GraphQLBundle\Error\UserError('Thread (with id:' . $id . ') not found.' );
        }

        return $res;
    }

    public function find($filters)
    {
        return $this->repo()->search($filters);
    }
    
}