<?php
namespace AppBundle\Resolver;

use Symfony\Component\HttpFoundation\ParameterBag;
use Doctrine\ORM\EntityManager;

class MessagesStatusResolver extends EmAwareResolver
{
	public function oldestMessageDate($peerId = null) {
        return $this->repo('AppBundle:Message')->oldestMessageDate($peerId);
    }
    public function newestMessageDate($peerId = null) {
        return $this->repo('AppBundle:Message')->newestMessageDate($peerId);
    }
    public function messagesCount($peerId = null) {
        return $this->repo('AppBundle:Message')->messagesCount($peerId);
    }
    public function cleanMessagesCount($peerId = null) {
        return $this->repo('AppBundle:Message')->cleanMessagesCount($peerId);
    }
    public function oldestThreadDate($peerId = null) {
        return $this->repo('AppBundle:Thread')->oldestThreadDate($peerId);
    }
    public function newestThreadDate($peerId = null) {
        return $this->repo('AppBundle:Thread')->newestThreadDate($peerId);
    }
    public function resolve($args) {
        $ret = [
            'id' => $args['peerId'],
            'peerId' => $args['peerId']
        ];
        return $ret;
    }
    
}