<?php
namespace AppBundle\Resolver;

use Symfony\Component\HttpFoundation\ParameterBag;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Item;
use AppBundle\Exceptions\UserErrorException;

class ItemsResolver extends EmAwareResolver
{

    public function repo($entityName = 'AppBundle:Item') {
        return parent::repo($entityName);
    }
    
    public function delete($args)
    {
        $item = $this->repo()->find( $args['id'] );
        $this->em->remove($item);
        $this->em->flush();
        return $this->repo()->findAll();
    }

    public function edit($args)
    {
        $item = $this->repo()->find( $args['id'] );
        $item->setName($args['name']);
        $this->em->flush();
        return $item;
    }

     public function register($args)
    {
        $name = $args['name'];
        $subtopicId = $args['subtopicId'];
        $subtopic = $this->repo('AppBundle:Subtopic')->find( $subtopicId );
        if(!$subtopic->getCanHaveItems()) {
            throw new UserErrorException('This subtopic is not allowed to have items.');
        }
        $item = new Item;
        $item->setName($name);
        $item->setSubtopic($subtopic);
        $this->em->persist($item);
        $this->em->flush();
        return $item;
    }

    public function findOne($args)
    {   
        $id = $args['id'];
        $item = $this->repo()->find( $id );
        if(!$item) throw new \RuntimeException("Item not found for id " . $id, 1);
        return $item;
    }

    public function find($args)
    {
        if( $args['subtopicId'] ) {
            $subtopic = $this->repo( 'AppBundle:Subtopic' )->find( $args['subtopicId'] );
            return $this->repo()->findBySubtopic($subtopic);
        }
        else {
            return $this->repo()->findAll();
        }
    }
    
}