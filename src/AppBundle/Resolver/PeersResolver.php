<?php
namespace AppBundle\Resolver;

use Symfony\Component\HttpFoundation\ParameterBag;
use Doctrine\ORM\EntityManager;

class PeersResolver extends EmAwareResolver
{

	public function repo($entityName = 'AppBundle:Peer') {
        return parent::repo($entityName);
    }

    public function find()
    {
        return $this->repo()->findAll();
    }
}