<?php
namespace AppBundle\Resolver;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Information;

class InformationsResolver extends EmAwareResolver
{
    public function repo($entityName = 'AppBundle:Information') {
        return parent::repo($entityName);
    }

	public function registerForThread($args)
    {
        $thread = $this->repo('AppBundle:Thread')->find( $args['threadId'] );
        $subtopic = $this->repo('AppBundle:Subtopic')->find( $args['information']['subtopicId'] );
        $about = $args['information']['about'];
        $info = new Information();
        $info->setSubtopic($subtopic);
        $info->setAbout($about);
        $thread->addInformation($info);
        $this->em->persist($info);
        $this->em->flush();
        $this->em->refresh($thread);
        return $thread;
    }

    public function remove($args)
    {
        $information = $this->repo()->find( $args['id'] );
        $thread = $information->getThread();
        $this->em->remove($information);
        $this->em->flush();
        $this->em->refresh($thread);
        return $thread;
    }
    
}