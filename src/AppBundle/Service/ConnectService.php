<?php

namespace AppBundle\Service;

use AppBundle\Entity\ConnectRequest;
use AppBundle\Entity\User;

class ConnectService
{
    /**
     * @var string
     */
    const LINK_MESSAGE = 'Seja bem vindo ao Nosso Jardim! Acesse pelo link: %s';

    /**
     * @var TelegramConnector
     */
    protected $telegramConnector;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $frontendUrl;

    /**
     * @var \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager
     */
    protected $tokenManager;

    /**
     * @param TelegramConnector $telegramConnector
     * @param \Doctrine\ORM\EntityManager $em
     * @param string $frontendUrl
     * @param \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager $tokenManager
     */
    public function __construct($telegramConnector, $em, $frontendUrl, $tokenManager)
    {
        $this->telegramConnector = $telegramConnector;
        $this->em = $em;
        $this->frontendUrl = $frontendUrl;
        $this->tokenManager = $tokenManager;
    }

    /**
     * @param TelegramConnector $telegramConnector
     * @return ConnectService
     */
    public function setTelegramConnector($telegramConnector)
    {
        $this->telegramConnector = $telegramConnector;
        return $this;
    }

    /**
     * @param array|string $payload
     */
    public function sendAccessLink($payload)
    {
        $this->em->beginTransaction();

        try {
            
            if (!is_array($payload)) {
                $payload = json_decode($payload, true);
            }

            if ($payload['message']['text'] != '/connect') {
                throw new \InvalidArgumentException('Invalid command.');
            }

            $connectRequest = $this->em->getRepository('AppBundle:ConnectRequest')->findOneBy([
                'updateId' => $payload['update_id']
            ]);

            if ($connectRequest) {
                throw new \InvalidArgumentException('Message already registered.');
            }

            $connectRequest = ConnectRequest::fromPayload($payload);

            $this->em->persist($connectRequest);

            $userData = $connectRequest->getJson()['message']['from'];

            $token = $this->createToken(
                $this->getUser(
                    $userData['id'],
                    trim($userData['first_name'] . ' ' . $userData['last_name'])
                )
            );

            $this->em->flush();

            $this->telegramConnector->sendMessage(
                $userData['id'],
                sprintf(self::LINK_MESSAGE, $this->getAccessLink($token))
            );
            
            $this->em->commit();

        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    /**
     * @param string $token
     * @return string
     */
    public function getAccessLink($token)
    {
        return $this->frontendUrl . '?bearer=' . $token;
    }

    /**
     * @param string $telegramUserId
     * @param string $name
     */
    protected function getUser($telegramUserId, $name)
    {
        $user = $this->em->getRepository('AppBundle:User')->findOneBy([
            'telegramId' => $telegramUserId
        ]);

        if (!$user) {
            $user = User::fromTelegram($telegramUserId, $name);
            $this->em->persist($user);
        }

        return $user;
    }

    /**
     * @param User $user
     * @return string
     */
    public function createToken($user)
    {
        return $this->tokenManager->create($user);
    }
}
