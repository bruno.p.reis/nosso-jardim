<?php

namespace AppBundle\Service;

class TelegramConnector
{
    /**
     * @var string
     */
    protected $botId;

    /**
     * @param string $botId
     */
    function __construct($botId)
    {
        $this->botId = $botId;
    }

    /**
     * @param string $method
     * @param array $params
     * @return string
     */
    public function url($method, $params)
    {
        $url = 'https://api.telegram.org/bot' . $this->botId . '/' . $method;
        $url .= '?' . http_build_query($params);
        return $url;
    }

    /**
     * @param string $filePath
     * @return string
     */
    public function getDownloadUrl($filePath)
    {
        return 'https://api.telegram.org/file/bot' . $this->botId . '/' . $filePath;
    }

    /**
     * @param string $chatId
     * @param string $text
     */
    public function sendMessage($chatId, $text)
    {
        file_get_contents(
            'https://api.telegram.org/bot' .
            $this->botId .
            '/sendMessage?text=' .
            urlencode($text) .
            '&chat_id=' .
            $chatId
        );
    }

    /**
     * @param string $fileId
     * @return mixed
     */
    public function getFile($fileId)
    {
        $url = $this->url('getFile', ['file_id' => $fileId]);
        return json_decode(file_get_contents($url));
    }
}
