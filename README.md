# Nosso Jardim
Rede social de informações sobre plantas.
## Instalação
- Clonar o projeto:

```bash
$ git clone https://gitlab.com/bruno.p.reis/nosso-jardim.git
```
- Entrar no diretório:

```
$ cd nosso-jardim
```
- Instalar as dependências:
 
```
$ php composer install
```
- Preencher os parâmetros de conexão com o banco de dados e a url de acesso ao client no arquivo `app/config/parameters.yml`.
 
- Criar a estrutura do banco de dados:
 
```
$ php bin/console doctrine:schema:create
```

- Importar os Media Types:
 
```
$ php bin/console doctrine:fixtures:load --append
```

## Execução

A API está exposta por meio da url: `http://nossojardim/graphql` (presumindo que o projeto esteja configurado no vhost "nossojardim")

Na seção Ferramentas estão listadas algumas opções para interagir com a API.


## Segurança
O sistema utiliza a tecnologia JSON Web Tokens - JWT - para transitar informações relativas a identidade e permissões do usuário.

### Autenticação
As requisições à API `graphql` devem conter o cabeçalho para idenficação do usuário:
```
Authorization: Bearer <token>
```

### Autorização
No momento, a aplicação conta com três perfis de acesso:
 - USER
 - CLASSIFIER
 - ADMIN

O perfil USER possui acesso somente-leitura (queries) e os demais possuem acesso de escrita (mutations). 
As atribuições dos perfis estão armazenadas na tabela `users_role`.

### Acesso
#### Produção
Para acessar o sistema, os usuários devem solicitar acesso ao bot telegram do Nosso Jardim por meio do comando `/connect`. Após a solicitação, o bot cadastrará o usuário no sistema, se for um usuário do telegram ainda inexistente na base, e retornará um link para acesso.
Mais informações sobre o funcionamento dos bots pode ser encontrada na seção BOT TELEGRAM e na documentação oficial do telegram, disponível na seção Referências.

#### Desenvolvimento
De modo a facilitar e agilizar o processo de desenvolvimento, foi criado um command que gera um token válido, de um usuário pré-existente, para ser utilizado com a API. Mais detalhes na seção de Commands.

## BOT Telegram
A aplicação dispõe de um bot para atuar como porta de entrada de novos usuários no sistema. O bot se comunica com a aplicação por meio de um `webhook` disponível na url `/connect`. Todo bot do telegram só se comunica com um único `webhook` visível publicamente na internet operando sobre o protoclo `https`. Por esse motivo, não é recomendável utilizar o bot em ambiente de desenvolvimento. 

Para configurar em produção, deve ser criado um bot específico para esse ambiente. A criação de bots é realizada por meio do @BotFather, disponível dentro do telegram. https://core.telegram.org/bots#3-how-do-i-create-a-bot

Após a criação do bot, o id do bot deve ser configurado na entrada `telegram_bot_token` do arquivo `app/config/parameters.yml`.

Para configurar o bot para se comunicar com a aplicação, a seguinte requisição deve ser realizada:
`https://api.telegram.org/bot<ID DO BOT>/setWebhook?url=<URL DE ACESSO>/connect`, onde `<ID DO BOT>` deve ser substituído pelo id do bot e `<URL DE ACESSO>` o link público de acesso do servidor do Nosso Jardim.

Se o procedimento tiver sido feito corretamente, os servidores do telegram responderão:
```json
{
  "ok": true,
  "result": true,
  "description": "Webhook was set"
}
```
Para fazer alterações no webhook e testar no ambiente de desenvolvimento, é sugerida a utilização da ferramenta NGROK.


## Coding Style

O código-fonte deve ser compatível com as recomendações propostas pelo [PHP-FIG](http://www.php-fig.org/psr/), com atenção especial para as PSRs 1, 2 e 4. 


## Workflow de desenvolvimento
O desenvolvimento do projeto segue o Feature Branch Workflow, preservando a branch `master` com o código fonte estável e pronto para utilização. Mais detalhes em: https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow

## Commands

Para dropar o schema do banco de dados atual:
```bash
$ php bin/console doctrine:schema:drop --force
```

Para atualizar o schema do banco de dados:
```bash
$ php bin/console doctrine:schema:update --force
```

Importar os Media Types:
```
$ php bin/console doctrine:fixtures:load --append
```

Para gerar um token de um usuário já existente:
```bash
$ php bin/console app:generate-access-token <userId>
```

Para importar as mensagens do telegram:
```bash
$ php bin/console app:import-telegram-messages <caminho/do/arquivo.json>
```

## Testes

O sistema possui testes de integração em sua maioria, porém testes puramente unitários podem ser aplicados em cenários complexos. Todos os testes estão dentro do diretório `tests`.

Para iniciar os testes, executar a seguinte instrução na raiz do projeto:
```bash
$ phpunit
```

Para executar somente os testes do GraphQL:
```bash
$ phpunit tests/AppBundle/GraphQL
```

Os testes de integração utilizam um banco SQLite para persistência. Se houver alguma alteração na estrutura do banco de dados, o cache dos testes deve ser apagado com o comando:
```bash
$ php bin/console cache:clear --env=test
```

## Diretrizes para escrita de testes

Decidimos por focar os testes principalmente na api, com testes de integração. Escreveremos testes unitários onde a lógica mais complexa de negócio exigir. No caso da integração os próprios testes devem se encarregar de criar a massa de dados, dando prioridade para o uso da própria api para fazê-lo. Helpers poderão ser criados para facilitar essa inicialização dos dados. 

Caso seja necessário uma massa de dados expressiva para os testes, que realmente não seja possível ou viável gerar pela api, vamos usar o faker. E, em última instância, criar fixtures direto no banco. 

### Alguns exemplos: 
 - RegisterThreadTest::shouldRegisterAThreadWithOneMessage & AddMessageToThreadResolverTest::shouldAddOneMessage  - Estes testam a camada graphQL. É o uso preferencial. Recomendado para todos os casos, salvo exceções justificadas. 
 - RegisterTelegramDumperMessage - Esse está testando direto a camada do resolver, sem usar a API. É um teste antigo do qual evoluimos para os testes usando a camada da API. Testes assim só devem ser feitos com justificativa para isso. 

### Helpers importantes (a documentação destes métodos tem boas dicas):
- AbstractIntegrationTestCase::processGraphQL
- AbstractIntegrationTestCase::gqlData
- AbstractIntegrationTestCase::gqlError
- AbstractIntegrationTestCase::gql_registerTelegramDumperMessage



## GraphQL API
Mutation para registrar uma mensagem
```
mutation{registerMessage(json:"{\"from_id\":123,\"message\":\"teste 3\",\"date\":1481113200}"){id}}
```
Para navegar na API, visualizar o Schema, querys e mutations, acesse `http://nossojardim/graphql/explorer`

## Clientes GraphQL
- https://github.com/graphql/graphiql
- https://github.com/redound/graphql-ide
- https://github.com/chentsulin/awesome-graphql#tools

## Referências
- http://www.php-fig.org/psr/
- http://symfony.com/doc/current/index.html
- http://symfony.com/doc/current/setup/web_server_configuration.html
- https://getcomposer.org/
- http://graphql.org/learn/
- https://github.com/Youshido/GraphQL
- https://github.com/Youshido/GraphQLBundle
- http://docs.doctrine-project.org/en/latest/
- https://phpunit.de/manual/current/en/index.html
- http://docs.mockery.io/en/latest/
- https://jwt.io/
- https://github.com/lexik/LexikJWTAuthenticationBundle
- https://core.telegram.org/bots
- https://ngrok.com/